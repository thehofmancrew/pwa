/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./frontend/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
    darkMode: "class", // or 'media' or 'class'
    theme: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
    ]
}

