# Pros and Cons

## Pros

* no need for node
* can have tailwind

## Cons

- no typescript/js - tried it but I cannot imagine working with `document.getElementById` ...

# TODO

- [ ] html design
- [ ] one example js function
- [ ] dont htmx --> new folder

## Create a tailwind.config.js file

./tailwindcss init

## Start a watcher

./tailwindcss -i input.css -o output.css --watch

## Compile and minify your CSS for production

./tailwindcss -i input.css -o output.css --minify