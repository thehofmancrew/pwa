module gitlab.com/BerndCzech/meterware/tailwind

go 1.21.0

require github.com/julienschmidt/httprouter v1.3.0

require github.com/go-chi/chi/v5 v5.0.10 // indirect
