package main

import (
	"context"
	"log"

	"gitlab.com/BerndCzech/meterware/tailwind/server"
)

func main() {
	ctx := context.Background()
	if err := server.Serve(ctx); err != nil {
		log.Fatalf("%+v", err)
	}
}
