package server

import (
	"context"
	"fmt"
	"net/http"
)

const (
	resourceFolder = "./frontend"

	localPort uint = 9999
)

func Serve(ctx context.Context) error {

	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir(resourceFolder)))

	return http.ListenAndServe(fmt.Sprintf(":%d", localPort), mux)
}
