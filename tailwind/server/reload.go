package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
)

/*
 compare: https://github.com/17twenty/go-tailwind-livereload

  go fileWatcher(ctx)
  r.GET("/reload.js", serveReloadJS)
  r.GET("/reload", setReloadState)
*/

var needsRefresh = false

const (
	refreshInterval = 1 * time.Second
	cssFolder       = resourceFolder
)

func serveTailwind(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	b, err := os.ReadFile(cssFolder)
	if err != nil {
		fmt.Fprintf(w, "could not read css: %+v", err)
		return
	}
	w.Write(b)
	return
}

func serveHTML(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	templateFileContents, err := os.ReadFile(resourceFolder + "/index.html")
	if err != nil {
		fmt.Fprintf(w, "Can't read file '%s' - %s", resourceFolder+"index.html", err)
		return
	}
	// Load and inject JS
	templateFileContents = []byte(strings.Replace(string(templateFileContents), "</head>", `<script type="text/javascript" src="/reload.js"></script></head>`, 1))

	_, err = w.Write(templateFileContents)
	if err != nil {
		fmt.Fprintf(w, "Could not write html: %+v", err)
	}
}

func setReloadState(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if needsRefresh {
		log.Println("Forcing reload")
		w.WriteHeader(http.StatusUpgradeRequired)
		return
	}
	// Weird quirk but with an empty response and status code for no content, Chrome still views it
	// as a failed load
	fmt.Fprint(w, "{}")
	return
}
func serveReloadJS(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprintf(w, fmt.Sprintf(`
		setInterval(function() {
			fetch("/reload")
			.then(function (res) {
				if (res.status == 426) {
					window.location.reload(true);
				}
			})
		}, %d);
		`, refreshInterval.Milliseconds()))

	needsRefresh = false
	return
}

func fileWatcher(ctx context.Context) error {
	fileWatch := map[string]time.Time{}
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(refreshInterval):
			//	happy path
		}

		f, err := os.Open(resourceFolder)
		if err != nil {
			log.Fatal("Couldnt open templateFolder", err)
		}
		files, _ := f.Readdir(-1)
		f.Close()

		for _, file := range files {
			info, _ := os.Stat(f.Name() + "/" + file.Name())
			if err != nil {
				// TODO: handle errors (e.g. file not found)
				log.Fatal("Couldnt stat file", err)
			}
			if info.ModTime().After(fileWatch[file.Name()]) {
				fileWatch[file.Name()] = info.ModTime()
				needsRefresh = true
			}
		}
	}

}
